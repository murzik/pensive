## Overview

Pensive is a CLI program for organizing knowledge similar to a wiki.
In this tool organization means to view, edit or remove information without leaving
the CLI.

## Description

The overall idea is to manage information with *tags* like `pacman`,
`python` or `python.sqlite3`. As there are different types of information
you might want to use, three essential formats are employed in pensive:

- Format 0: Plain text, shows everything at once, e.g. command references
- Format 1: Separate entries which can be unfolded, e.g. some scripts
- Format 2: Attachments, e.g. URLs, files or other tags

In pensive, one can create *categories* which are linked to the mentioned format
numbers. In case any information about a specific tag is saved in a category, 
it will be displayed in the precise order, in which the categories have
been installed. Categories also determine the way one interacts with pensive:
They can be addressed by appropriate upper case letters starting from
`A` in the pensive shell.

Please note that there is a tutorial included, which can be viewed at
the initial start of the program.

## Installation

`git clone https://gitlab.com/murzik/pensive.git`

## Usage

```
cd pensive
python3 pensive.py
```

## Files

    pensive.py        the program
    pensive.conf      plain text configuration file
    pensive.sqlite    sqlite database, which will be created at the first start
    pensive.temp      a plain text dump file for editing that might be created
    LICENSE           GPLv3

## Examples
### Help view

```
python3 pensive
:help
basic functions:
    add TAG [TAG2]      - add tag(s)
    rm TAG [TAG2]       - remove tag(s) with all its entries
    mv TAG1 TAG2        - rename TAG1 to TAG2
    ls [*PAT*ERN*]      - list all tags [matching * globbed pattern]
    ?TAG                - query tag, activates tag mode
    ??PATTERN           - search for pattern in all tags and their entries

additional tag mode functions:
    A                   - show result of the 1st category
    B0                  - open entry/attachment of a category (format 1, 2)
    *A                  - edit or create entry of a category (format 0)
    *B0                 - edit or create entry of a category (format 1, 2)
    -A                  - remove existing entry (format 0)
    -B0                 - remove existing entry (format 1, 2)
    mv A E              - move entry to category with equal format
    mv B0 F             - move entry to category with equal format
    mv B0 F@TAG         - move entry to category with equal format of TAG

manage pensive functions:
    category show       - show all defined categories
    category add NAME 0 - add category NAME with the formatting 0 to pensive
    category rm NAME    - remove the category NAME from pensive
    category mv OLD NEW - renames a category from OLDNAME to NEWNAME
    backup [NAME]       - backup pensive's current state [as NAME]
    restore             - restore pensive by choosing an old backup out of a list
    export              - export each tag with its entries to plain text
```

### Show user defined categories and the associated format number

```
python3 pensive
:category show
defined categories:
    A = Beschreibung (0)
    B = Kommandoreferenz (0)
    C = Notizen (1)
    D = Tutorials (2)
    E = Verweise (2)
```

### Format 0: plain text
```
python3 pensive
:?cargo                                     // query tag named `cargo`
Kommandoreferenz:
    cargo new --lib mylib
    cargo new --bin myfoo
    cargo clippy            # lint code
    cargo fmt               # format code
    cargo fix               # apply suggested fixes
    ...
```

### Format 1: separate entries

```
python3 pensive 
:?nc                                        // query tag named `nc`

--snip--

Notizen:
    [0] remote shell
    [1] test network speed
    ...

[nc]:C1                                     // Unfold entry 1 of the 3rd category
test network speed:
    # set up nc to listen on remotehost (here 192.168.2.2)
    nc -l 2112 > /dev/null

    # send data from host to remotehost and display bandwidth
    dd if=/dev/zero count=10000000 | nc -v 192.168.2.2 2112
```

### Format 2: attachments, tags and hyperlinks
```
python3 pensive 
:?rust

--snip--

Verweise:
    [0] Rust book
        https://doc.rust-lang.org/book/
    [1] CLI: argument parsing, unit and integration testing with I/O, files..
        https://rust-lang-nursery.github.io/cli-wg/index.html
    [2] Standard build / project management tool
        cargo

[rust]: E0                                  // open link in the browser
[rust]: E2                                  // switch to pensive tag named `cargo`
```